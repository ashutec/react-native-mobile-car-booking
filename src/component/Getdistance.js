import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Container, CardItem, Body, Card } from 'native-base';

export default class GetDistance extends Component {

    render() {

        const { routeTime,
            distance
        } = this.props
        return (
            <View>
                {distance && routeTime &&
                    < Container style={styles.getDistance} >
                        <Card style={styles.card}>
                            <CardItem>
                                <Body>
                                    <Text>Duration:{' '}{routeTime}</Text>
                                    <Text>Distance:{' '}{distance}</Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </Container >
                }
            </View>

        )
    }
}

const styles = StyleSheet.create({
    getDistance: {
        /*   top: 550,
        width: '100%',
        position: 'absolute',
        fontSize: 20, */
        /* alignContent: 'center',
        alignItems: 'center', */
        // justifyContent: 'flex-end',
        marginTop: 400,
        flex: 1,
        bottom: 0,
        backgroundColor: 'transparent',
        flexDirection: 'row',
        alignSelf: 'center',


    },

    card: {
        // justifyContent: 'center',
        width: '95%',
        height: 100,
        alignSelf: 'center',
    }

})


