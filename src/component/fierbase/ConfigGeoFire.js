// import React, { Component } from 'react';
// import { Text, View, StyleSheet, Dimensions } from 'react-native';
// import MapView from 'react-native-maps'
// import GeoFire from 'geofire'
// import * as firebase from "firebase";
// import fierbaseConfig from './fierbaseConfig';
// require("firebase/firestore");

// const { width, height } = Dimensions.get('window');
// const ASPECT_RATIO = width / height;
// const LATITUDE_DELTA = 0.0922;
// const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

// export default class ConfigGeoFire extends Component {
//     constructor(Props) {
//         super(Props)
//         this.state = {
//             initialPosition: null
//         }
//     }

//     componentWillMount() {
//         firebase.initializeApp(fierbaseConfig);
//         firebase.firestore().settings({
//             timestampsInSnapshots: true
//         });

//         navigator.geolocation.getCurrentPosition(
//             position => {
//                 const lat = parseFloat(position.coords.latitude)
//                 const long = parseFloat(position.coords.longitude)
//                 const initialRegion = {
//                     latitude: lat,
//                     longitude: long,
//                     latitudeDelta: LATITUDE_DELTA,
//                     longitudeDelta: LONGITUDE_DELTA,
//                 }
//                 this.setState({ initialPosition: initialRegion, markerPosition: initialRegion })
//             },
//             (error) => console.log(error.message),

//             { enableHighAccuracy: true, timeout: 2000, maximumAge: 1000000 }
//         );

//         this.storeQuary()

//     }

//     storeQuary = () => {

//         const firebaseRef = firebase.database().ref("-LVCJrrnMT-u3gC3oto6");

//         // Create a new GeoFire instance at the random Firebase location
//         const geoFire = new GeoFire(firebaseRef);


//         const carsLocation = [{
//             name: "BMW",
//             id: 1,
//             location: [23.00751360033381, 72.49751692995756]
//         }, {
//             name: "SUV",
//             id: 2,
//             location: [23.011927827024486, 72.5067201228876]
//         }, {
//             name: "Ciaz",
//             id: 3,
//             location: [23.006466637322536, 72.51233206618258]
//         },
//         {
//             name: "Mercedes",
//             id: 4,
//             location: [22.998318679964562, 72.52648835390346]

//         },
//         {
//             name: "Jaguar",
//             id: 5,
//             location: [23.02804202810781, 72.4919771166285]
//         },
//         {
//             name: "Range_Rover",
//             id: 6,
//             location: [23.046307540330066, 72.51621280609447],
//         },
//         {
//             name: "Land_Rover",
//             id: 7,
//             location: [23.051559636804814, 72.53756564252126]
//         },
//         {
//             name: "Hummar",
//             id: 8,
//             location: [23.0984093630511, 72.53171549827562]
//         }
//         ]

//         const geoQuery = geoFire.query({
//             center: [22.99974087075545, 72.49932717509682],
//             radius: 10
//         });

//         carsLocation.map(function (cords, index) {
//             geoFire.set(cords.name, cords.location, cords.id, `IndexOn:${index}`).then(function () {
//                 console.log(cords.name, cords.location, cords.id);
//             });
//         });

//         /*  var onReadyRegistration = geoQuery.on("ready", function () {
//              console.log("GeoQuery has loaded and fired all other events for initial data");
//          }); */

//         geoQuery.on("key_entered", this.setCarsLocation);



//         /* var onKeyExitedRegistration = geoQuery.on("key_exited", function (key, location, distance) {
//             console.log(key + " exited query to " + location + " (" + distance + " km from center)");
//         }); */

//         /*   var onKeyMovedRegistration = geoQuery.on("key_moved", function (key, location, distance) {
//               console.log(key + " moved within query to " + location + " (" + distance + " km from center)");
//           }); */

//     }

//     setCarsLocation = (key, location, distance) => {
//         this.setState({ distance, key, location })
//         console.log(key + " entered query at " + location + " (" + distance + " km from center)");
//     }

//     getCurrentLocation = () => {

//         this.watchID = navigator.geolocation.watchPosition(
//             position => {
//                 const lat = parseFloat(position.coords.latitude)
//                 const long = parseFloat(position.coords.longitude)
//                 const lastRegion = {
//                     latitude: lat,
//                     longitude: long,
//                     latitudeDelta: LATITUDE_DELTA,
//                     longitudeDelta: LONGITUDE_DELTA,
//                 }
//                 this.setState({ initialPosition: lastRegion, markerPosition: lastRegion })
//             }
//         );
//     }

//     render() {

//         console.log(this.state.location)

//         return (
//             <MapView
//                 ref={map => (this._mapView = map)}
//                 style={styles.map}
//                 showsUserLocation={true}
//                 showsPointsOfInterest={false}
//                 followsUserLocation={true}
//                 provider="google"
//                 region={this.state.initialPosition}
//             >


//             </MapView >
//         );
//     }
// }

// const styles = StyleSheet.create({
//     map: {
//         flex: 1,
//         alignItems: 'center',
//         justifyContent: 'center',
//         backgroundColor: '#ecf0f1',
//     }
// });