import React, { PureComponent } from 'react'
import { View, Text } from 'native-base';
import { AsyncStorage } from 'react-native'
import firebase from 'react-native-firebase'



export default class PushNotification extends PureComponent {
    async componentDidMount() {
        this.checkPermission();
    }

    //1
    async checkPermission() {
        firebase.messaging().hasPermission()
            .then(enabled => {
                if (enabled) {
                    console.log('permission granted')
                    this.getToken(); // user has permissions
                } else {
                    console.log('permission request')
                    this.requestPermission();   // user doesn't have permission
                }
            });

    }

    //3
    getToken = async () => {
        const fcmToken = await AsyncStorage.getItem('fcmToken', value);
        console.log('before fcmToken', fcmToken)
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                // user has a device token
                console.log('after fcmToken', fcmToken)
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
    }

    //2
    async requestPermission() {

        firebase.messaging().requestPermission()
            .then(() => {
                console.log('permission granted in requestPermission ')
                this.getToken();   // User has authorised  
            })
            .catch(error => {
                console.log('permission rejected in requestPermission');  // User has rejected permissions  
            });
    }

    render() {

        return (
            <View>
            </View>
        )
    }
}