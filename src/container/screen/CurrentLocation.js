import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Dimensions, YellowBox } from 'react-native'
import MapView from 'react-native-maps'
import SearchResult from '../../component/SearchResult';
import GetDistance from '../../component/Getdistance';
import * as firebase from "firebase";
import fierbaseConfig from '../../component/fierbase/fierbaseConfig';
import PushNotification from './PushNotify';
require("firebase/firestore");
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class CurrentLocation extends Component {
    constructor(props) {
        super(props)
        YellowBox.ignoreWarnings(['Setting a timer']);
    }

    render() {
        const childProps = {
            pickUpAutoSuggest: this.props.pickUpAutoSuggest,
            selectPickUpAddress: this.props.selectPickUpAddress,
            selectDropAddress: this.props.selectDropAddress,
            dropAutoSuggest: this.props.dropAutoSuggest,
            distance: this.props.distance,
            routeTime: this.props.routeTime
        }

        const {
            enterPickUpLocation,
            getPickUpLocationByAdress,
            getDropLocationByAdress,
            initialPosition,
            markerPosition,
            setLocation,
            regionChange,
            coordinate,
            setLocationByAddress,
            focusOnDrop,
            enterDropLocation,
            destinationMarkar,
            pickUpPosition,
            carsCoordinate,
            geoCars
        } = this.props

        const GeoCars = [];
        for (const key in geoCars) {
            if (geoCars.hasOwnProperty(key)) {
                const element = geoCars[key];
                GeoCars.push(element);
            }
        }



        return (
            < React.Fragment >
                <View>
                    <TextInput style={styles.input}
                        value={enterPickUpLocation}
                        keyboardType='web-search'
                        clearTextOnFocus
                        placeholder="Enter Pick up Location"
                        onChangeText={(text) => getPickUpLocationByAdress(text)}
                        onFocus={setLocationByAddress}
                    />

                    <TextInput style={styles.input}
                        value={enterDropLocation}
                        keyboardType='web-search'
                        clearTextOnFocus
                        placeholder="Enter Drop Location"
                        onChangeText={(text) => getDropLocationByAdress(text)}
                        autoFocus={focusOnDrop}
                        multiline={true}
                    />
                </View>

                <MapView
                    style={styles.map}
                    region={initialPosition}
                    showsUserLocation
                    onRegionChangeComplete={(cords) => regionChange(cords)}
                    followsUserLocation
                    zoomControlEnabled
                    onPress={setLocation}
                    loadingEnabled={true}
                    moveOnMarkerPress={true}
                    showsCompass={true}
                >

                    {markerPosition &&
                        < MapView.Marker
                            draggable
                            coordinate={markerPosition}
                        />
                    }

                    {pickUpPosition &&
                        <MapView.Marker
                            coordinate={pickUpPosition}
                        />
                    }

                    {coordinate &&
                        <MapView.Polyline
                            coordinates={coordinate}
                            strokeColors={['black', 'black']}
                            strokeWidth={4}
                        />
                    }

                    {destinationMarkar &&
                        <MapView.Marker
                            coordinate={destinationMarkar}
                            pinColor='blue'
                        />
                    }

                    {carsCoordinate.length > 0 &&
                        carsCoordinate.map((cords, index) =>
                            < MapView.Marker
                                key={index}
                                title={cords.id}
                                coordinate={{
                                    latitude: cords.lat,
                                    longitude: cords.long,
                                    LATITUDE_DELTA,
                                    LONGITUDE_DELTA
                                }}
                                image={require('../../../assets/108440.png')}
                            />)
                    }

                    {GeoCars.length > 0 &&
                        GeoCars.map((cords, index) =>
                            < MapView.Marker
                                key={index}
                                coordinate={{
                                    latitude: cords.location[0],
                                    longitude: cords.location[1],
                                    LATITUDE_DELTA,
                                    LONGITUDE_DELTA
                                }}
                                title={`${cords.key}${'  '} ${cords.distance} KM`}
                                image={require('../../../assets/clipart-top-view-xar-86c8.png')} />)
                    }

                </MapView>
                <PushNotification />

                <SearchResult {...childProps} />
                <GetDistance {...childProps} />
            </React.Fragment >
        )
    }
}

const styles = StyleSheet.create({

    input: {
        padding: 10,
        opacity: 0.9,
        marginLeft: 20,
        marginRight: 10,
        fontSize: 18,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#48BBEC',
        backgroundColor: 'white',
        marginTop: 15
    },

    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: -1,
    },

    calloutView: {
        flexDirection: "row",
        backgroundColor: "white",
        borderRadius: 10,
        width: "100%",
    },

});


