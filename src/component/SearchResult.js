import React, { Component } from 'react'
import { StyleSheet, Text } from 'react-native'
import { List, ListItem, Icon, Left, Content, Card, CardItem, Body } from 'native-base';

export default class SearchResult extends Component {
    constructor(props) {
        super(props)
        this.state = {
            searchList: []
        }
    }
    render() {

        const {
            pickUpAutoSuggest,
            selectPickUpAddress,
            selectDropAddress,
            dropAutoSuggest
        } = this.props

        return (
            <React.Fragment>
                {pickUpAutoSuggest && pickUpAutoSuggest.length > 0 &&
                    < List style={styles.searchResultWrapper}>
                        {pickUpAutoSuggest.map(add =>
                            <ListItem style={{ marginTop: 10, marginBottom: 10 }}
                                onPress={() => selectPickUpAddress(add)} avatar key={Math.random()}>
                                <Left style={styles.leftWrapper}>
                                    <Icon type="MaterialIcons" style={styles.icon} name='location-on' />
                                </Left>
                                <Text style={{ fontSize: 20, marginTop: 10 }}> {add[0] + ',' + add[2]}</Text>
                            </ListItem>
                        )}
                    </ List>
                }
                {dropAutoSuggest && dropAutoSuggest.length > 0 &&
                    < List style={styles.searchResultWrapper}>
                        {dropAutoSuggest.map(add =>
                            <ListItem style={{ marginTop: 10, marginBottom: 10 }}
                                onPress={() => selectDropAddress(add)} avatar key={Math.random()}>
                                <Left style={styles.leftWrapper}>
                                    <Icon type="MaterialIcons" style={styles.icon} name='location-on' />
                                </Left>
                                <Text style={{ fontSize: 20, marginTop: 10 }}> {add[0] + ',' + add[2]}</Text>
                            </ListItem>)}
                    </ List>}

            </React.Fragment >
        )
    }
}

const styles = StyleSheet.create({
    searchResultWrapper: {
        top: 180,
        position: 'absolute',
        backgroundColor: '#fff',
        opacity: 0.9,
        width: '100%',
        fontSize: 20,
        borderRadius: 10,
        height: 1000,
    },
    searchList: {
        height: 0
    },
    leftWrapper: {
        flexWrap: 'wrap',
        alignItems: 'flex-start',

    },
    icon: {
        marginLeft: 10,
        marginRight: 10,
        color: '#1a1b29'
    }
})
