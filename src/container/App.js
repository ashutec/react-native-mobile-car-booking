import React, { Component } from 'react'
import { Dimensions, StyleSheet, Keyboard } from 'react-native'
import Polyline from '@mapbox/polyline'
import * as Rx from 'rxjs'
import { distinctUntilChanged, debounceTime, switchMap } from 'rxjs/operators';
import { Container } from 'native-base';
import CurrentLocation from './screen/CurrentLocation';
import GeoFire from 'geofire'
import fierbaseConfig from '../component/fierbase/fierbaseConfig';
import * as firebase from "firebase";
import ConfigGeoFire from '../component/fierbase/ConfigGeoFire';
import PushNotification from './screen/PushNotify';
require("firebase/firestore");
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const APIKEY = 'AIzaSyC-GVEzhiktPXdamcjCpdPRN7NMl7Nbm9c'
const URL = `https://maps.googleapis.com/maps/api/`

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      enterPickUpLocation: '',
      enterDropLocation: '',
      pickUpAutoSuggest: null,
      dropAutoSuggest: null,
      focusOnDrop: false,
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0,
      },
      geoCars: {},
      markerPosition: {
        latitude: 0,
        longitude: 0
      },
      carsCoordinate: [],
    };
  }

  getPickUpLocationByAdress$ = new Rx.Subject();
  getDropLocationByAdress$ = new Rx.Subject()

  componentDidMount() {
    firebase.initializeApp(fierbaseConfig)
    this.getCurrentLocation()
    this.subscribeLocationList()
    // this.animationCarLocation()
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
    if (this.PickUpLocation || this.DropLocation) {
      this.PickUpLocation.unsubscribe();
      this.DropLocation.unsubscribe();
    }
  }

  subscribeLocationList = () => {
    this.PickUpLocationSub = this.getPickUpLocationByAdress$
      .pipe(debounceTime(100), distinctUntilChanged(), switchMap(pickUp => Rx.from(this.updatePickUpPlaceList(pickUp))))
      .subscribe(pickUpAutoSuggest => this.setState({ pickUpAutoSuggest }));

    this.DropLocationSub = this.getDropLocationByAdress$
      .pipe(debounceTime(100), distinctUntilChanged(), switchMap(drop => Rx.from(this.updateDropPlaceList(drop))))
      .subscribe(dropAutoSuggest => this.setState({ dropAutoSuggest }));
  }

  // get current Location
  getCurrentLocation = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        const lat = parseFloat(position.coords.latitude)
        const long = parseFloat(position.coords.longitude)
        const initialRegion = {
          latitude: lat,
          longitude: long,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }
        this.getCurrentAddress(lat, long)
        this.setState({ initialPosition: initialRegion, markerPosition: initialRegion })
      },
      (error) => console.log(error.message),

      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000000 }
    );
    this.watchID = navigator.geolocation.watchPosition(
      position => {
        const lat = parseFloat(position.coords.latitude)
        const long = parseFloat(position.coords.longitude)
        const lastRegion = {
          latitude: lat,
          longitude: long,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }
        this.setState({ initialPosition: lastRegion, markerPosition: lastRegion })
      }
    );

  }

  // Get Drop Location
  getDropLocationByAdress = (text) => {
    this.setState({ enterDropLocation: text })
    this.getDropLocationByAdress$.next(text)
  }

  updateDropPlaceList = async (text) => {
    const lat = this.state.initialPosition.latitude
    const long = this.state.initialPosition.longitude
    const url = `${URL}place/autocomplete/json?input=${text}&location=${lat},${long}&radius=5000&key=${APIKEY}`
    const response = await fetch(url).then(res => res.json());
    const dropAutoSuggest = response.predictions.map(Add => Add.terms.map(add => add.value))
    return dropAutoSuggest
  }

  // Get PickUp Location
  getPickUpLocationByAdress = (text) => {
    this.setState({ enterPickUpLocation: text })
    this.getPickUpLocationByAdress$.next(text)
  }

  updatePickUpPlaceList = async (text) => {
    const lat = this.state.initialPosition.latitude
    const long = this.state.initialPosition.longitude
    const url = `${URL}place/autocomplete/json?input=${text}&location=${lat},${long}&radius=5000&key=${APIKEY}`
    const response = await fetch(url).then(res => res.json());
    const pickUpAutoSuggest = response.predictions.map(Add => Add.terms.map(add => add.value))
    return pickUpAutoSuggest
  }

  getCurrentAddress = async (lat, long) => {
    const url = `${URL}geocode/json?address=${lat},${long}&key=${APIKEY}`
    const { results } = await fetch(url).then(res => res.json());
    const enterPickUpLocation = results.length && results[0].formatted_address ? results[0].formatted_address : '';
    this.setState({
      enterPickUpLocation,
      geoCars: []
    })
    this.storeQuary(lat, long);
  }

  selectPickUpAddress = (value) => {
    this.setState({
      enterPickUpLocation: value[0],
      pickUpAutoSuggest: []
    })
    this.getPickUpAddress(value)
  }

  getPickUpAddress = async (value) => {
    const address = value.map(add => add)
    const url = `${URL}geocode/json?address=${address}&key=${APIKEY}`
    const response = await fetch(url).then(res => res.json());
    this.setState({
      initialPosition: {
        latitude: response.results[0].geometry.location.lat,
        longitude: response.results[0].geometry.location.lng,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      pickUpPosition: {
        latitude: response.results[0].geometry.location.lat,
        longitude: response.results[0].geometry.location.lng,
        longitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      markerPosition: null,
      focusOnDrop: true
    })
    Keyboard.dismiss()
  }

  //  Set Drop Location
  selectDropAddress = (value) => {
    this.setState({
      enterDropLocation: value[0],
      dropAutoSuggest: []
    })
    this.getDropAdress(value)
  }

  getDropAdress = async (value) => {
    const address = value.map(add => add)
    const url = `${URL}geocode/json?address=${address}&key=${APIKEY}`
    const response = await fetch(url).then(res => res.json());

    this.setState({
      destinationMarkar: {
        latitude: response.results[0].geometry.location.lat,
        longitude: response.results[0].geometry.location.lng,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }

    })
    const location = response.results[0].geometry.location
    this.getRoute(location)
    Keyboard.dismiss()
  }

  setLocation = async (region) => {
    this.setState({
      markerPosition: region.nativeEvent.coordinate,
      initialPosition: {
        latitude: region.nativeEvent.coordinate.latitude,
        longitude: region.nativeEvent.coordinate.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }
    })
    const latitude = region.nativeEvent.coordinate.latitude
    const longitude = region.nativeEvent.coordinate.longitude
    const url = `${URL}geocode/json?address=${latitude},${longitude}&key=${APIKEY}`
    const response = await fetch(url).then(res => res.json());
    {
      response.results[0].address_components[3] &&
        this.setState({ enterPickUpLocation: response.results[0].formatted_address })
    }
  }

  // Get Route between two point
  getRoute = async (location) => {
    const mode = 'driving';
    const originLatitude = this.state.initialPosition.latitude ? this.state.initialPosition.latitude : this.state.pickUpPosition.latitude
    const originLongitude = this.state.initialPosition.longitude ? this.state.initialPosition.longitude : this.state.pickUpPosition.longitude
    const destination = `${location.lat}, ${location.lng}`;
    const originLocation = `${originLatitude},${originLongitude}`;
    const url = `${URL}directions/json?origin=${originLocation}&destination=${destination}&key=${APIKEY}&mode=${mode}`;
    const response = await fetch(url).then(res => res.json());

    if (response.routes.length) {
      const distance = response.routes[0].legs[0].distance.text;
      const routeTime = response.routes[0].legs[0].duration.text;
      const coords = Polyline.decode(response.routes[0].overview_polyline.points)
      const coordinate = coords.map(cords => {
        return {
          latitude: cords[0],
          longitude: cords[1]
        }
      })
      this.setState({
        northeast: {
          latitude: response.routes[0].bounds.northeast.lat,
          longitude: response.routes[0].bounds.northeast.lng,
        },
        southwest: {
          latitude: response.routes[0].bounds.southwest.lat,
          longitude: response.routes[0].bounds.southwest.lng,
        },
        coordinate, routeTime, distance
      })
    }
  }

  setLocationByAddress = () => {
    this.setState({
      enterPickUpLocation: ""
    })
  }

  regionChange = (cords) => {
    const lat = cords.latitude
    const long = cords.longitude
    this.getCurrentAddress(lat, long)
    this.setState({
      markerPosition: cords,
      initialPosition: cords
    })

  }

  animationCarLocation = () => {

    firebase.database().ref('cars').on('value', (snapshot) => {
      this.setCarsLocation(snapshot.val())
    });
  }

  setCarsLocation = (coordinate) => {
    const carsCoordinate = Object.values(coordinate)
    this.setState({ carsCoordinate })
    // this.updateCarsLocation(carsCoordinate)
  }

  updateCarsLocation = (carsCoordinate) => {
    // console.log(carsCoordinate)

  }

  storeQuary = (lat, long) => {
    const firebaseRef = firebase.database().ref("-LVCJrrnMT-u3gC3oto6");
    const geoFire = new GeoFire(firebaseRef);

    const geoQuery = geoFire.query({
      center: [lat, long],
      radius: 2
    });
    geoQuery.on("key_entered", this.getGeoCarsLocation);
  }

  getGeoCarsLocation = (key, location, distance) => {
    if (location !== undefined) {
      this.setState((prevState) => ({ geoCars: { [key]: { location, key, distance }, ...prevState.geoCars } }))
    }
  }

  render() {
    childProps = {
      pickUpAutoSuggest: this.state.pickUpAutoSuggest,
      selectPickUpAddress: this.selectPickUpAddress,
      initialPosition: this.state.initialPosition,
      markerPosition: this.state.markerPosition,
      getPickUpLocationByAdress: this.getPickUpLocationByAdress,
      regionChange: this.regionChange,
      enterPickUpLocation: this.state.enterPickUpLocation,
      enterDropLocation: this.state.enterDropLocation,
      setLocationByAddress: this.setLocationByAddress,
      focusOnDrop: this.state.focusOnDrop,
      getDropLocationByAdress: this.getDropLocationByAdress,
      selectDropAddress: this.selectDropAddress,
      dropAutoSuggest: this.state.dropAutoSuggest,
      destinationMarkar: this.state.destinationMarkar,
      coordinate: this.state.coordinate,
      cameraPosition: this.state.cameraPosition,
      pickUpPosition: this.state.pickUpPosition,
      southWest: this.state.southWest,
      northEast: this.state.northEast,
      routeTime: this.state.routeTime,
      distance: this.state.distance,
      carsCoordinate: this.state.carsCoordinate,
      geoCars: this.state.geoCars
    }
    return (
      <React.Fragment>
        <Container style={styles.container}>
          <CurrentLocation {...childProps} />
        </Container>
      </React.Fragment >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    position: 'absolute',
    width: '100 %',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1,
  },
})
